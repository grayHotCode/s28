
// #3
fetch('https://jsonplaceholder.typicode.com/todos')
	.then((response) => response.json())
	.then((json) => console.log(json));

// #5
fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response) => response.json())
	.then((json) => console.log(json));


//# 7
fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			title: 'New Todos',
			completed: true,
			userId: 2
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))


//# 8 
fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			userId: 1,
    		id: 1,
    		title: "this is a PUT",
    		completed: true
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))


// #9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			id: 1,
			title: 'Updated Todos',
			description: 'this is a description',
			status: 'status',
			date_completed: '10-25-2021',
			userId: 2
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))


// #10
fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method: 'PATCH',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			title: 'Corrected Todos',
			description: 'Corrected description'
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


// #11 
fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			id: 1,
			title: 'Updated Todos',
			description: 'this is a description',
			status: 'Complete',
			date_completed: '10-25-2021',
			userId: 2
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))



fetch('https://jsonplaceholder.typicode.com/todos/2')
	.then((response) => response.json())
	.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			title: 'New Post',
			completed: false,
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))


